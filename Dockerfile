FROM alpine:3.11

ENV USER=wwwuser

# add php-alpine repository pub key
ADD https://php.hernandev.com/key/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub

# make sure you can use HTTPS, add repository
RUN apk --update-cache add ca-certificates && \
    echo "https://php.hernandev.com/v3.11/php-7.4" >> /etc/apk/repositories

# Install packages
RUN apk --no-cache add php php-fpm php-opcache php-openssl php-curl \
    nginx supervisor curl

# https://github.com/codecasts/php-alpine/issues/21
RUN ln -s /usr/bin/php7 /usr/bin/php

# Configure nginx
COPY config/nginx.conf /etc/nginx/nginx.conf

# Remove default server definition
RUN rm /etc/nginx/conf.d/default.conf

# Configure PHP-FPM
COPY config/fpm-pool.conf /etc/php7/php-fpm.d/www.conf
COPY config/php.ini /etc/php7/conf.d/custom.ini

# Configure supervisord
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Setup document root
RUN mkdir -p /var/www/html

# Create a non-root user
RUN addgroup -S $USER && adduser \
    --disabled-password \
    --gecos "" \
    --ingroup $USER \
    --no-create-home \    
    $USER

# Make sure files/folders needed by the processes are accessable when they run under the $USER
RUN chown -R $USER.$USER /var/www/html && \
  chown -R $USER.$USER /run && \
  chown -R $USER.$USER /var/lib/nginx && \
  chown -R $USER.$USER /var/log/nginx

# Switch to use a non-root user from here on
USER $USER

# Add application
WORKDIR /var/www/html
COPY --chown=wwwuser src/ /var/www/html/

# Expose the port nginx is reachable on
EXPOSE 8080

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping
